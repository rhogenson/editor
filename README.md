# A terrible text editor

## Controls:
  - Arrow keys to move the cursor
  - CTRL-Q: quit
  - CTRL-S: save
  - CTRL-Z: undo
  - CTRL-Y: redo
